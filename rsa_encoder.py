from Crypto.Cipher import PKCS1_OAEP
from Crypto.PublicKey import RSA
import base64

def encrypt_with_public_key(a_message, public_key):
	encryptor = PKCS1_OAEP.new(public_key)
	encrypted_msg = encryptor.encrypt(a_message)
	encoded_encrypted_msg = base64.b64encode(encrypted_msg)
	return encoded_encrypted_msg

x509pem = """-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhy409fkyzMbJ4aVtv7m6ythfZwTaqSBsj28qoMrkvbg1OPzqcWLlHSVW4GkNpwGDnOi+5iV9zy7JcrBZo6THqBouFFfP7ezDMa6eL4yFFdBmTt5xnEfHK7be0WQ3nJM2lJOeZ3XHS2jtngP9Do6bkwaQs3KucuW8DRGFe1WRSI2cDO0gwS4rYqUxw/j9pu8sJORSfPEwkvufCagfHoEUzCuz2q6xOFbZzgFObM6jnGPLy53d+ezACcrsnVAjCfw+Vb/cPCjgH+WXpjbFgMzQ4g0nZPYOOBUyvJMEvVhX/k79PQBNq9m3gGdGgweeeqNMdYrV5cxxi4upPn3JQy0kLQIDAQAB
-----END PUBLIC KEY-----"""
# x509pem = open('public.pem', 'r').read() # load the key alternatively from the file system

key = RSA.import_key(x509pem)      # add the key import with import_key() or importKey()
paylodas = open('PayloadsAllTheThings/SQL\ Injection/Intruder/Auth_Bypass.txt','r+')
for lines in paylodas:
	byte_message = bytes(lines,'utf-8')
	print(byte_message)
	encoded_encrypted_msg = encrypt_with_public_key(byte_message, key)
	print(encoded_encrypted_msg.decode('utf-8'))
